/**
 * OBJETOS LITERALES
 */
const alumno = {
    nombre: 'Estefany',
    Apellido: 'Paco',
    Edad: 28,
    padres: {
        mama: 'Mariana',
        papa: 'Alberto'
    }
}
console.log(typeof alumno);
console.log(alumno.padres);
console.log(alumno['Apellido']);
    // Asignar mas datos a un objeto y reasignar
alumno.genero = 'femenino'
console.log(alumno); // al asignar otro atributo el cambio se genera automaticamente y en cualquier parte del codigo se imprima antes o despues de anadir mas atributos
alumno.Edad = 29     // de la misma manera se reasignacion un atributo, pero los cambios se guardan de acuerdo al flujo del codigo
console.log(alumno);

    // Notaciones ES6
const universidad = 'Mayor de San Simon';
const facultad = 'Ciencias y tecnologias';
const carrera = 'Ingenieria';

const estudios = {
    universidad,
    facultad,
    carrera
}

console.log(estudios) // se puede asignar variables como atributos dentro de un objeto y resumir su nomenclatura

    // ATRIBUTOS Y METODOS
    // los atributos son los datos dentro de un objeto
    // los metodos es la manera de como llamar a los atributos de un objeto, no son funciones pero si tienen la misma nomenclatura
const fiesta = {
    torta: 'Ana',
    vasos: 'Carol',
    sillas: 'Laura',
    numeroDeAmigos: 56,
    datosDeAnfitrion: {
        nombres: 'Paola',
        apellidos: 'Ramirez Serrano',
        edad: 25
    },
    anuncio() {
        console.log(`Se realizo una fiesta donde ${this.torta} es madrina de torta y ${this.datosDeAnfitrion.nombres} es el anfitrion`)
    }
}
fiesta.anuncio()
